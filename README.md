# 归因分类中间件
#### 介绍
归因组件ACE：订单归类技术解决方案。基于淘宝团队归因中间件设计思想，超强的分类归因中间件工具。 https://blog.csdn.net/Taobaojishu/article/details/109505977

单元化：条件抽象、分类器黑盒设计理念、执行器原子化。整个流程做到了单元化，各模块之间耦合度极低，实现了对条件的编排。归因器很好的支持了修改、扩展，解决了if...else的嵌套难题。
 


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
