package sample;

import ace.constant.Constants;
import ace.core.AceContext;
import ace.core.AceResult;
import ace.core.AceWorker;
import ace.executor.IExecutor;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import sample.scene.AceScene;

import java.util.List;

@Slf4j
public class SampleService {

    public static void HealthSettleBiz() throws Exception {
        AceWorker aceWorker = AceWorker.getInstance();
        JSONObject dataParam = new JSONObject() {{
            put(Constants.TAG,"health_topic");
        }};

        //构建分类器的上下文
        AceContext context = AceContext.of(dataParam, AceScene.ACE_SCENE_HEALTH_SETTLE);
        AceResult<List<IExecutor>> aceClassifyResult = aceWorker.classify(context);

        //分类器返回的数据就是对应分类器返回的数据了，客户端程序可以自定义
        log.info("classify result:{}", JSON.toJSONString(aceClassifyResult));

        aceClassifyResult.ifPresent((result) -> {
            //构造执行器的上下文， 这里scene就没有作用了
            AceContext executorAceContext = AceContext.of(result, AceScene.ACE_SCENE_HEALTH_SETTLE);
            List<AceResult> aceExecuteResultList =  aceWorker.execute(executorAceContext);
            log.debug("executor list result is :{}", JSON.toJSONString(aceExecuteResultList));
            aceExecuteResultList.stream().forEach(o -> {
                log.debug("result:{}",o.getResult());
            });
        });

    }


    public static void hotelRefundOrderSettleBiz() throws Exception {
        AceWorker aceWorker = AceWorker.getInstance();

        //构建业务参数
        JSONObject dataParam = new JSONObject() {{
            put(Constants.orderType,"2");
            put(Constants.TAG,"health_topic");
        }};

        //构建分类器的上下文
        AceContext context = AceContext.of(dataParam, AceScene.ACE_SCENE_HOTEL_SETTLE);
        AceResult<List<IExecutor>> aceClassifyResult = aceWorker.classify(context);

        //分类器返回的数据就是对应分类器返回的数据了，客户端程序可以自定义
        log.info("classify result:{}", JSON.toJSONString(aceClassifyResult));

        aceClassifyResult.ifPresent((result) -> {
            //构造执行器的上下文， 这里scene就没有作用了
            AceContext executorAceContext = AceContext.of(result, AceScene.ACE_SCENE_HOTEL_SETTLE);
            List<AceResult> aceExecuteResultList =  aceWorker.execute(executorAceContext);
            log.debug("executor list result is :{}", JSON.toJSONString(aceExecuteResultList));
            aceExecuteResultList.stream().forEach(o -> {
                log.debug("result:{}",o.getResult());
            });
        });

    }

    public static void hotelConsumeOrderSettleBiz() throws Exception {

        AceWorker aceWorker = AceWorker.getInstance();

        //构建业务参数
        JSONObject dataParam = new JSONObject() {{
            put(Constants.orderType,"hotel");
            put(Constants.TAG,"order_finsh");
        }};

        //构建分类器的上下文
        AceContext context = AceContext.of(dataParam, AceScene.ACE_SCENE_HOTEL_SETTLE);
        AceResult<List<IExecutor>> aceClassifyResult = aceWorker.classify(context);

        //分类器返回的数据就是对应分类器返回的数据了，客户端程序可以自定义
        log.info("classify result:{}", JSON.toJSONString(aceClassifyResult));

        aceClassifyResult.ifPresent((result) -> {
            //构造执行器的上下文， 这里scene就没有作用了
            AceContext executorAceContext = AceContext.of(result, AceScene.ACE_SCENE_HOTEL_SETTLE);
            List<AceResult> aceExecuteResultList =  aceWorker.execute(executorAceContext);
            log.debug("executor list result is :{}", JSON.toJSONString(aceExecuteResultList));
            aceExecuteResultList.stream().forEach(o -> {
                log.debug("result:{}",o.getResult());
            });
        });
    }
}
