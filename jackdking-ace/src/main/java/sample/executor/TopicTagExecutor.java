package sample.executor;

import ace.annoation.Executor;
import ace.core.AceContext;
import ace.core.AceResult;
import ace.executor.IExecutor;

/**
 * Copyright (C) 阿里巴巴
 *
 * @ClassName TopicTagExecutor
 * @Description TODO
 * @Author jackdking
 * @Date 11/08/2021 12:14 下午
 * @Version 2.0
 **/
@Executor(name = "TopicTagExecutor")
public class TopicTagExecutor implements IExecutor {
    @Override
    public AceResult execute(AceContext aceContext) {
        return new AceResult(true,"execute TopicTagExecutor");
    }
}
