package sample.scene;

import com.google.common.collect.ImmutableList;


/**
 * @Description: 分类场景，一个场景对应一个或多个分类器, 如果分类器分类结果有重复则抛出异常
 * @Param:
 * @return:
 * @Author: jackdking
 * @User: 10421
 * @Date: 2022/10/8
 **/
public enum AceScene {

    ACE_SCENE_HOTEL_SETTLE("hotel_settle",ImmutableList.of("orderTagClassifier", "refundOrderClassifier"))
    ,
    ACE_SCENE_HEALTH_SETTLE("health_settle",ImmutableList.of("healthVoucherClassifier"));

    public String sceneName;
    public ImmutableList<String> classifierList;

    AceScene(String sceneName , ImmutableList<String> classifierList){
        this.sceneName = sceneName;
        this.classifierList = classifierList;
    }
}
