package ace;

import ace.annoation.Attributor;
import ace.attributor.IAttributor;
import ace.constant.Constants;
import ace.core.AceContext;
import ace.core.AceResult;
import ace.core.AceWorker;
import ace.executor.IExecutor;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import sample.SampleService;
import sample.classifier.RefundOrderClassifier;
import sample.scene.AceScene;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/*
 * 应用场景
 * 1. 高德app订单中心数据查询平台
 */
@Slf4j
@SpringBootApplication
public class Application implements ApplicationRunner  {


    public static void main( String[] args )
    {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        SampleService.HealthSettleBiz();
        SampleService.hotelConsumeOrderSettleBiz();
        SampleService.hotelRefundOrderSettleBiz();

    }

}

