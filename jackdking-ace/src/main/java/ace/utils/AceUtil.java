package ace.utils;

import ace.constant.Constants;
import ace.core.AceResult;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class AceUtil {

    public static String getClassifierRulerName (String classifierName, String rulerName, String matcher) {
        return classifierName+ Constants.SPLITER+rulerName+matcher;
    }

    public static AceResult aggregateResult (List<AceResult> aceResults) {
        AceResult retResult = new AceResult(true,null);
        for(AceResult aceResult:aceResults) {
            retResult = retResult.and(aceResult);
        }
        return retResult;
    }

    public static AceResult aggregateResult (AceResult... aceResults) {
        AceResult retResult = new AceResult(true,null);
        for(AceResult aceResult:aceResults) {
            retResult = retResult.and(aceResult);
        }
      List<JSONObject> dataList = Lists.newArrayList();
      Optional.ofNullable(dataList).orElse(Collections.emptyList())
        .stream()
        .forEach(data -> data.remove(""));

      return retResult;
    }

    public static AceResult aggregateResult (List<AceResult> aceResults , String aggregateRuler) {
        return null;
    }

    public static List<AceResult> negativeAceResultList(List<AceResult> aceResults){

        aceResults.stream().forEach(aceResult -> aceResult.negative());
        return aceResults;
    }
}
